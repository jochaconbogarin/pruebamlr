class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.text :guest_title

      t.timestamps
    end
  end
end
