class CreateLinkRoomFacilities < ActiveRecord::Migration
  def change
    create_table :link_room_facilities, id: false do |t|
      t.references :room, index: true
      t.references :facility, index: true
      t.text :details

    end
  end
end
