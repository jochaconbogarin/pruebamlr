class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :amount
      t.string :comments
      t.references :payment_method, index: true
      t.references :booking, index: true

      t.timestamps
    end
  end
end
