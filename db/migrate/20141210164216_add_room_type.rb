class AddRoomType < ActiveRecord::Migration
  def self.up
    add_column :room_types, :room_type, :string
  end
  def self.down
    remove_column :room_types, :room_type
  end
end
