class AddDetailsToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :title, :string
    add_column :customers, :name, :string
    add_column :customers, :email, :string
  end
end
