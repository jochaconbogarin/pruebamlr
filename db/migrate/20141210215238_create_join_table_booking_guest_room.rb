class CreateJoinTableBookingGuestRoom < ActiveRecord::Migration
  def change
  	create_table :bookings_guests_rooms, :id => false do |t|
  		t.references :room, index: true
  		t.references :guest, index: true
  		t.references :booking, index: true
    end
  end
end
