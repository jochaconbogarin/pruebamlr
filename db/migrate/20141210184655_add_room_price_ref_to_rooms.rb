class AddRoomPriceRefToRooms < ActiveRecord::Migration
  def change
    add_reference :rooms, :room_price, index: true
  end
end
