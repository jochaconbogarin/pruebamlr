class CreateRoomBands < ActiveRecord::Migration
  def change
    create_table :room_bands do |t|
      t.string :band_desc

      t.timestamps
    end
  end
end
