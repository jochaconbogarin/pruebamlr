class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :floor

      t.timestamps
    end
  end
end
