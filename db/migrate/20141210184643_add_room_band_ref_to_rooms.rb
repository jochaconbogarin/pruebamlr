class AddRoomBandRefToRooms < ActiveRecord::Migration
  def change
    add_reference :rooms, :room_band, index: true
  end
end
