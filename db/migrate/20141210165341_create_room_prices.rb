class CreateRoomPrices < ActiveRecord::Migration
  def change
    create_table :room_prices do |t|
      t.integer :current_room_price

      t.timestamps
    end
  end
end
