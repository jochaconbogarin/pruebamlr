class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :total
      t.date :due
      t.references :customer, index: true

      t.timestamps
    end
  end
end
