# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141210215238) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bookings", force: true do |t|
    t.integer  "total"
    t.date     "due"
    t.integer  "customer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bookings", ["customer_id"], name: "index_bookings_on_customer_id", using: :btree

  create_table "bookings_guests_rooms", id: false, force: true do |t|
    t.integer "room_id"
    t.integer "guest_id"
    t.integer "booking_id"
  end

  add_index "bookings_guests_rooms", ["booking_id"], name: "index_bookings_guests_rooms_on_booking_id", using: :btree
  add_index "bookings_guests_rooms", ["guest_id"], name: "index_bookings_guests_rooms_on_guest_id", using: :btree
  add_index "bookings_guests_rooms", ["room_id"], name: "index_bookings_guests_rooms_on_room_id", using: :btree

  create_table "customers", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.string   "name"
    t.string   "email"
  end

  create_table "facilities", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  create_table "guests", force: true do |t|
    t.text     "guest_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "link_room_facilities", id: false, force: true do |t|
    t.integer "room_id"
    t.integer "facility_id"
    t.text    "details"
  end

  add_index "link_room_facilities", ["facility_id"], name: "index_link_room_facilities_on_facility_id", using: :btree
  add_index "link_room_facilities", ["room_id"], name: "index_link_room_facilities_on_room_id", using: :btree

  create_table "payment_methods", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  create_table "payments", force: true do |t|
    t.integer  "amount"
    t.string   "comments"
    t.integer  "payment_method_id"
    t.integer  "booking_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["booking_id"], name: "index_payments_on_booking_id", using: :btree
  add_index "payments", ["payment_method_id"], name: "index_payments_on_payment_method_id", using: :btree

  create_table "room_bands", force: true do |t|
    t.string   "band_desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_prices", force: true do |t|
    t.integer  "current_room_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_types", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "room_type"
  end

  create_table "rooms", force: true do |t|
    t.string   "floor"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "room_type_id"
    t.integer  "room_band_id"
    t.integer  "room_price_id"
  end

  add_index "rooms", ["room_band_id"], name: "index_rooms_on_room_band_id", using: :btree
  add_index "rooms", ["room_price_id"], name: "index_rooms_on_room_price_id", using: :btree
  add_index "rooms", ["room_type_id"], name: "index_rooms_on_room_type_id", using: :btree

end
