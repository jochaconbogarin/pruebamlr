class Payment < ActiveRecord::Base
  has_one :payment_method

  validates :amount, numericality: {:greather_than => 500, message: "Must be greather than 500"}
  validates_presence_of :comments, :payment_method, :booking, :amount
end
