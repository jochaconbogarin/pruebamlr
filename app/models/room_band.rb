class RoomBand < ActiveRecord::Base
  belongs_to :room

  validates :band_desc,
    length:{ minimum: 15,
             maximum: 50,
             too_short: 'must have at least %{count} characters',
             too_long: 'must have at most %{count} characters'},
    presence: true
end
