class Booking < ActiveRecord::Base

#Associations

  belongs_to :customer
  has_one :payment

  has_and_belongs_to_many :guests
  has_and_belongs_to_many :rooms

#Validations


  validates :total, presence: {  message: "The total field is mandatory" }, numericality: { :greater_than_or_equal_to => 2000, message: "Must me more than 2000"}
  validates :customer_id, presence: true
  validates :due,   presence: true
  validate  :due_validation

  private

  def due_validation
  	if self.due && ( self.due > Date.today + 6.months || self.due.past? )
  		return errors.add(:due, "Due date can't be in the past nor six months ahead from today.")
  	end
  end
end
