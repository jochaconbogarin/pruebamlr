class RoomType < ActiveRecord::Base
  belongs_to :room

  validates :room_type,
    presence: true,
    length: { minimum: 5,
              maximum: 20,
              message: 'Must be greather than 5 and less than 20 characters.' }
end
