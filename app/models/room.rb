
class Room < ActiveRecord::Base
  has_many :link_room_facilities
  has_many :facilities, through: :link_room_facilities
  has_one :room_band
  has_one :room_price
  has_one :room_type
  has_and_belongs_to_many :guests
  has_and_belongs_to_many :bookings

  validates_presence_of :floor, :room_type, :room_price, :room_band
end
