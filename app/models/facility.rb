class Facility < ActiveRecord::Base
  has_many :link_room_facilities
  has_many :rooms, through: :link_room_facilities
end
