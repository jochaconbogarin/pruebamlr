class PaymentMethod < ActiveRecord::Base
	belongs_to :payments

  validates :name, presence: true
end
