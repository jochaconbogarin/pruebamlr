class RoomPrice < ActiveRecord::Base
  belongs_to :room

  validates :current_room_price, presence: true,
    numericality: { only_integer: true,
                    greater_than: 500,
                    less_than: 20000
                  }
end
