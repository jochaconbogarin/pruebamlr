class Customer < ActiveRecord::Base

#Associations

	has_many :bookings

#Validations

	validates :name,  presence:   { message: "The name field is mandatory" }, 
					  length:     { within: 10..30, 
					  					too_short: "Name must at least 10 characters long.", 
					  					too_long: "Name must be at most 30 characters long." }

	validates :title, allow_nil:  true,
					  inclusion:  { in: %w(Mr. Mrs. Ms. Jr.), 
									message: "Title must be: Mr., Mrs., Ms. or Jr." }
					  

	validates :email, presence:   { message: 'The email field is mandatory' }, 
			          uniqueness: { message: 'That email has already been taken.'}, 
			          format:     { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, 
			          		        	on: :create,
			          		        	message: 'Input text must be a valid email address.' }
end