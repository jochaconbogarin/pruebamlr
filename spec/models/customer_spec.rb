require 'rails_helper'

RSpec.describe Customer, :type => :model do

	describe 'validations' do

		describe 'name' do

			context 'when name is longer than 30 characters' do
				it 'invalid' do
			  		testCustomer  = Customer.new

			  		testCustomer.name  = 'Daniel Alberto Leguizamón Santamaría'
			  		testCustomer.title = 'Mr.'
			  		testCustomer.email = 'dleguizamon@ejemplo.com'

			  		testCustomer.save

			  	   	expect(testCustomer.errors[:name]).to eq(['Name must be at most 30 characters long.'])
				end
			end

			context 'when name is shorter than 10 characters' do
				it 'invalid' do
					testCustomer = Customer.new

					testCustomer.name = 'Daniel'
					testCustomer.title = 'Mr.'
					testCustomer.email = 'dleguizamon@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:name]).to eq(['Name must at least 10 characters long.'])
				end
			end

			context 'when no name is given' do
				it 'invalid' do
					testCustomer = Customer.new

					testCustomer.title = 'Mr.'
					testCustomer.email = 'dleguizamon@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:name]).to eq(['The name field is mandatory', 'Name must at least 10 characters long.'])
				end
			end

			context 'when name is 10 characters' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana Isabel'
					testCustomer.title = 'Ms.'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:name]).to eq([])
				end
			end

			context 'when name is 30 characters' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.title = 'Ms.'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:name]).to eq([])
				end
			end
		end

		describe 'title' do
			context 'when title is Mr.' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.title = 'Mr.'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:title]).to eq([])
				end
			end

			context 'when title is Mrs.' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.title = 'Mrs.'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:title]).to eq([])
				end
			end

			context 'when title is Ms.' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.title = 'Ms.'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:title]).to eq([])
				end
			end

			context 'when title is Jr.' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.title = 'Jr.'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:title]).to eq([])
				end
			end

			context 'when title is not in the list' do
				it 'invalid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.title = 'S'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:title]).to eq(['Title must be: Mr., Mrs., Ms. or Jr.'])
				end
			end

			context 'when title is not given' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Ana María Isabel de los Santos'
					testCustomer.email = 'aisabel@ejemplo.com'

					testCustomer.save

					expect(testCustomer.errors[:title]).to eq([])
				end
			end
		end

		describe 'email' do

			context 'when email is not present' do
				it 'invalid' do
					testCustomer = Customer.new

					testCustomer.name = 'Alejandro Gómez Vásquez'
					testCustomer.email = ''

					testCustomer.save

					expect(testCustomer.errors[:email]).to eq(['The email field is mandatory', 'Input text must be a valid email address.'
						])
				end
			end

			context 'when email is not valid' do
				it 'invalid' do
					testCustomer = Customer.new

					testCustomer.name = 'Alejandro Gómez Vásquez'
					testCustomer.email = 'algova@gmail'

					testCustomer.save

					expect(testCustomer.errors[:email]).to eq(['Input text must be a valid email address.'])
				end
			end

			context 'when email is valid' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Alejandro Gómez Vásquez'
					testCustomer.email = 'algova@gmail.com'

					testCustomer.save

					expect(testCustomer.errors[:email]).to eq([])
				end
			end

			context 'when email has more than one domain address' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Alejandro Gómez Vásquez'
					testCustomer.email = 'algova@ucr.ac.cr'

					testCustomer.save

					expect(testCustomer.errors[:email]).to eq([])
				end
			end

			context 'when email already exists' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Alejandro Gómez Vásquez'
					testCustomer.email = 'algova@ucr.ac.cr'

					testCustomer.save

					sameEmailCustomer = Customer.new

					sameEmailCustomer.name = 'Álvaro González Valerio'
					sameEmailCustomer.email = 'algova@ucr.ac.cr'

					sameEmailCustomer.save

					expect(sameEmailCustomer.errors[:email]).to eq(['That email has already been taken.'])
				end
			end

			context 'when email has spaces' do
				it 'valid' do
					testCustomer = Customer.new

					testCustomer.name = 'Alejandro Gómez Vásquez'
					testCustomer.email = 'alg ova@ucr.ac.cr'

					testCustomer.save

					expect(testCustomer.errors[:email]).to eq(['Input text must be a valid email address.'])
				end
			end
		end
	end
end
