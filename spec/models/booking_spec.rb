require 'rails_helper'

RSpec.describe Booking, :type => :model do

  describe "validations" do

    describe "due" do
      context "when it is within two months" do
        it "valid" do
          	   testBooking  = Booking.new

          	   testBooking.total = 25000
          	   testBooking.due   = Date.parse("3-2-2015")
          	   testBooking.customer_id = 2

          	   expect(testBooking.save).to eq(true)
        end
      end

      context "when it is within two months and has no customer_id" do

          it "invalid" do
            testBooking  = Booking.new

            testBooking.total = 25000
            testBooking.due   = Date.parse("3-2-2015")
            testBooking.save

            expect(testBooking.errors[:customer_id]).to eq(["can't be blank"])
          end
      end

      context "when it is within a year and a half" do
        it "invalid" do
             testBooking  = Booking.new
             testBooking.total = 25000
             testBooking.due   = Date.parse("3-6-2016")

            testBooking.save

            expect(testBooking.errors[:due]).to eq(["Due date can't be in the past nor six months ahead from today."])
        end
      end

      context "when it is three months in the past" do
        it "invalid" do
             testBooking  = Booking.new
             testBooking.total = 25000
             testBooking.due   = Date.parse("16-8-2014")

             testBooking.save

             expect(testBooking.errors[:due]).to eq(["Due date can't be in the past nor six months ahead from today."])
        end
      end

      context "when it is within three months" do
        it "valid" do
          testBooking  = Booking.new
          testBooking.total = 25000
          testBooking.due   = Date.parse("21-3-2015")
          testBooking.customer_id = 6

          expect(testBooking.save).to eq(true)
        end
      end

    end 

    describe "total" do
      context "when it is less than 1500" do
        it "invalid" do
          testBooking  = Booking.new
          testBooking.total = 1500
          testBooking.due   = Date.parse("21-3-2015")
          testBooking.customer_id = 6

          testBooking.save

          expect(testBooking.errors[:total]).to eq(["Must me more than 2000"])
        end
      end

      context "when it is negative" do

        it "invalid" do
          testBooking  = Booking.new
          testBooking.total = -1500
          testBooking.due   = Date.parse("21-3-2015")
          testBooking.customer_id = 6

          testBooking.save

          expect(testBooking.errors[:total]).to eq(["Must me more than 2000"])
        end
      end
    end

  end
end