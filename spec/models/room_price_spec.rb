require 'rails_helper'

RSpec.describe RoomPrice, :type => :model do
  describe 'validations' do
    describe '#current_room_price' do

      context 'when is a number' do
        it 'is valid' do
          price = RoomPrice.create(current_room_price:5000)
          expect(price.errors[:current_room_price][0]).to eq nil
        end
      end

      context 'when is a double' do
        it 'is invalid' do
          price = RoomPrice.create(current_room_price:5.0)
          expect(price.errors[:current_room_price][0]).to eq 'must be an integer'
        end
      end

      context 'when is a string' do
        it 'is invalid' do
          price = RoomPrice.create(current_room_price:'Amarillo')
          expect(price.errors[:current_room_price][0]).to eq 'is not a number'
        end
      end

      context 'when is greather than 20000' do
        it 'is invalid' do
          price = RoomPrice.create(current_room_price:21000)
          expect(price.errors[:current_room_price][0]).to eq 'must be less than 20000'
        end
      end

      context 'when is less than 500' do
        it 'is invalid' do
          price = RoomPrice.create(current_room_price:400)
          expect(price.errors[:current_room_price][0]).to eq 'must be greater than 500'
        end
      end

    end
  end
end
