require 'rails_helper'

RSpec.describe RoomBand, :type => :model do
  describe 'validations' do
  	describe '#band_desc' do

      context 'when is empty' do
        it 'is invalid' do
          band = RoomBand.create
          expect(band.errors[:band_desc][1]).to eq 'can\'t be blank'
        end
      end

      context 'when is largest than 50 characters' do
        it 'is invalid' do
          band = RoomBand.create(band_desc:'Its a great room, it is a little short for the needs of a single person, but its ok for anybody. Enjoy it!!!')
          expect(band.errors[:band_desc][0]).to eq 'must have at most 50 characters'
        end
      end

      context 'when is smallest than 15 characters' do
        it 'is invalid' do
          band = RoomBand.create(band_desc:'Its nice!')
          expect(band.errors[:band_desc][0]).to eq 'must have at least 15 characters'
        end
      end

  	end
  end
end
