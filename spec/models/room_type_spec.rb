require 'rails_helper'

RSpec.describe RoomType, :type => :model do
  describe 'validations' do
    describe '#room_type' do
      context 'when has less than 5 characters' do
        it 'is invalid' do
          accommodation = RoomType.create(room_type:'Casa')
          expect(accommodation.errors[:room_type][0]).to eq 'Must be greather than 5 and less than 20 characters.'
        end
      end
      context 'when has more than 20 characters' do
        it 'is invalid' do
          accommodation = RoomType.create(room_type: 'ImperialStandardDouble')
          expect(accommodation.errors[:room_type][0]).to eq 'Must be greather than 5 and less than 20 characters.'

        end
      end
      context 'when has more than 5 characters' do
        it 'is valid' do
          accommodation = RoomType.create(room_type:'Standard')
          expect(accommodation.errors[:room_type][0]).to eq nil
        end
      end
      context 'when is a nil object' do
        it 'is invalid' do
          accommodation = RoomType.create
          expect(accommodation.errors[:room_type][0]).to eq 'can\'t be blank'
        end
      end

    end
  end
end
